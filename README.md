# Monitoring bundle

This is a Symfony bundle used for integrating talentrydev/locking library into a Symfony project.

## Installing

* Run:

```
composer require talentrydev/locking-bundle
```

* Add the LockingBundle to your kernel's `registerBundles` method:

```
return [
    //...
    new \Talentry\LockingBundle\LockingBundle();
];
```
