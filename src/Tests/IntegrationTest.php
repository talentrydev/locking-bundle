<?php

declare(strict_types=1);

namespace Talentry\LockingBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Talentry\Locking\Lock;

class IntegrationTest extends KernelTestCase
{
    private Lock $lock;

    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->lock = self::$container->get(Lock::class);
    }

    public function testIntegration()
    {
        self::assertTrue($this->lock->acquire('test'));
        self::assertTrue($this->lock->release('test'));
    }
}
