<?php

declare(strict_types=1);

namespace Talentry\LockingBundle\Tests;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;
use Talentry\LockingBundle\LockingBundle;
use Talentry\MonitoringBundle\MonitoringBundle;

class TestKernel extends Kernel
{
    public function registerBundles()
    {
        return [
            new FrameworkBundle(),
            new MonitoringBundle(),
            new DoctrineBundle(),
            new MonologBundle(),
            new LockingBundle(),
        ];
    }

    public function getProjectDir()
    {
        return __DIR__ . '/../../';
    }

    public function getRootDir()
    {
        return $this->getProjectDir();
    }

    public function getCacheDir()
    {
        return $this->getProjectDir() . '/var/cache/' . $this->getEnvironment();
    }

    public function getLogDir()
    {
        return $this->getProjectDir() . '/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__ . '/config.yml');
    }

    public function getName()
    {
        return 'TestKernel';
    }
}
