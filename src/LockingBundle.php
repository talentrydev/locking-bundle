<?php

declare(strict_types=1);

namespace Talentry\LockingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LockingBundle extends Bundle
{
}
